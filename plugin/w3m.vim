b
"AUTHOR:   Seve Tessarin 
"WEBSITE:  https://gitlab.com/Sevepy/vim-w3m
"VERSION:  0.0.2, for Vim 8.0+
"LICENSE:  MIT



" Don't source the plug-in when it's already been loaded or &compatible is set.
if &cp || exists('g:loaded_w3m')
  finish
endif


" Configuration defaults. {{{1

if !exists('g:w3m_binary_path')
  let g:w3m_binary_path = 'C:\\msys64\\usr\\bin\\'
endif


if !exists('g:w3m_start_page')
  let g:w3m_start_page = 'https://tessarinseve.pythonanywhere.com/shared/mozilla-thunderbird-start-page.html'

endif

if !exists('g:w3m_verify_urls')
  " Set this to true if your URLs include significant trailing punctuation and
  " your Vim is compiled with Python support. XXX In this case the shell
  " plug-in will perform HTTP HEAD requests on your behalf.
  let g:w3m_verify_urls = 0
endif


augroup PluginW3m

  " These enable automatic highlighting of URLs and e-mail addresses.
  autocmd! BufNew,BufRead,Syntax * call w3m#highlight_urls()
augroup END


command! -bar -nargs=? -complete=file Open call w3m#open_cmd(<q-args>)

" Default key mappings. {{{1

nnoremap <leader>oo :Open<CR>

" Make sure the plug-in is only loaded once.
let g:loaded_w3m = 1
