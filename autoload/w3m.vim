" Vim auto-load script

let g:w3m#version = '0.0.2'

function! w3m#openfile(location, ...) " {{{1
  if xolox#misc#os#is_win()
      let command = '!start cmd /c start "" %s'
      silent execute printf(command, xolox#misc#escape#shell(a:location))
    return
  elseif xolox#misc#os#is_mac()
    call xolox#misc#msg#debug("vim-misc %s: detected mac os x, using 'open' command to open %s ..", g:xolox#misc#version, string(a:location))
    let cmd = 'open ' . shellescape(a:location) . ' 2>&1'
    call s:handle_error(cmd, system(cmd))
    return
  else
    for handler in s:handlers + a:000
      if executable(handler)
        call xolox#misc#msg#debug("vim-misc %s: using '%s' to open '%s'.", g:xolox#misc#version, handler, a:location)
        let cmd = shellescape(handler) . ' ' . shellescape(a:location) . ' 2>&1'
        call s:handle_error(cmd, system(cmd))
        return
      endif
    endfor
  endif
  throw printf(s:enoimpl, g:xolox#misc#version, 'xolox#misc#open#file')
endfunction

function! w3m#openurl(url) " {{{1
  let url = a:url
  if url !~ '^\w\+://'
    call xolox#misc#msg#debug("vim-misc %s: the url %s doesn't contain a scheme, improvising ..", g:xolox#misc#version, string(url))
    if url !~ '@'
      call xolox#misc#msg#debug("vim-misc %s: defaulting to http:// url scheme ..", g:xolox#misc#version)
      let url = 'http://' . url
    elseif url !~ '^mailto:'
      call xolox#misc#msg#debug("vim-misc %s: defaulting to mailto: url scheme ..", g:xolox#misc#version)
      let url = 'mailto:' . url
    endif
  endif
  let on_unix = has('unix')
  let not_on_mac = !xolox#misc#os#is_mac()
  let no_gui_available = (has('gui_running') == 0 && $display == '')
        execute '!start wt.exe -M ' . g:w3m_binary_path . '\\w3m.exe ' fnameescape(url)
        call s:handle_error('w3m.exe' . ' ' . url, '')
        return
  call xolox#misc#msg#debug("vim-misc %s: defaulting to gui web browser to open %s ..", g:xolox#misc#version, string(url))
  call w3m#openfile(url, 'firefox', 'google-chrome')
endfunction

function! s:handle_error(cmd, output) " {{{1
  if v:shell_error
    let message = "vim-misc %s: failed to execute program! (command line: %s%s)"
    let output = strtrans(xolox#misc#str#trim(a:output))
    if output != ''
      let output = ", output: " . string(output)
    endif
    throw printf(message, g:xolox#misc#version, a:cmd, output)
  endif
endfunction

" vim: et ts=2 sw=2 fdm=marker
function! w3m#open_cmd(arg) " {{{1
  " Implementation of the :Open command.
  try
    " No argument?
    if a:arg !~ '\S'
      " Filename, URL or e-mail address at text cursor location?
      if !s:open_at_cursor()
        "open start page
        execute '!start wt.exe -M ' . g:w3m_binary_path . 'w3m.exe ' fnameescape(g:w3m_start_page)

        " Open the directory of the current buffer.
        "let bufdir = expand('%:p:h')
        "call xolox#misc#msg#debug("w3m %s: Opening directory of current buffer '%s'.", g:w3m#version, bufdir)
        "call w3m#openfile(bufdir)
      endif
    elseif (a:arg =~ w3m#url_pattern()) || (a:arg =~ w3m#mail_pattern())
      " Open the URL or e-mail address given as an argument.
      call xolox#misc#msg#debug("w3m %s: Opening URL or e-mail address '%s'.", g:w3m#version, a:arg)
      call w3m#openurl(a:arg)
    else
      let arg = fnamemodify(a:arg, ':p')
      " Does the argument point to an existing file or directory?
      if isdirectory(arg) || filereadable(arg)
        call xolox#misc#msg#debug("w3m %s: Opening valid filename '%s'.", g:w3m#version, arg)
        call w3m#openfile(arg)
      else
        let msg = "I don't know how to open '%s'! %s"
        echoerr printf(msg, a:arg, s:contact)
      endif
    endif
  catch
    call xolox#misc#msg#warn("w3m %s: %s at %s", g:w3m#version, v:exception, v:throwpoint)
  endtry
endfunction

function! s:open_at_cursor()
  let cWORD = expand('<cWORD>')
  " Start by trying to match a URL in <cWORD> because URLs can be more-or-less
  " unambiguously distinguished from e-mail addresses and filenames.
  if g:w3m_verify_urls && cWORD =~ '^\(http\|https\)://.\{-}[[:punct:]]$' && w3m#url_exists(cWORD)
    let match = cWORD
  else
    let match = matchstr(cWORD, w3m#url_pattern())
  endif
  if match != ''
    call xolox#misc#msg#debug("w3m %s: Matched URL '%s' in cWORD '%s'.", g:w3m#version, match, cWORD)
  else
    " Now try to match an e-mail address in <cWORD> because most filenames
    " won't contain an @-sign while e-mail addresses require it.
    let match = matchstr(cWORD, w3m#mail_pattern())
    if match != ''
      call xolox#misc#msg#debug("w3m %s: Matched e-mail address '%s' in cWORD '%s'.", g:w3m#version, match, cWORD)
    endif
  endif
  if match != ''
    call w3m#openurl(match)
    return 1
  else
    call xolox#misc#msg#debug("w3m %s: Trying to match filename in current line ..", g:w3m#version)
    " As a last resort try to match a filename at the text cursor position.
    let line = getline('.')
    let idx = col('.') - 1
    let match = matchstr(line[0 : idx], '\f*$')
    let match .= matchstr(line[idx+1 : -1], '^\f*')
    " Expand leading tilde and/or environment variables in filename?
    if match =~ '^\~' || match =~ '\$'
      let match = split(expand(match), "\n")[0]
    endif
    if match != '' && (isdirectory(match) || filereadable(match))
      call xolox#misc#msg#debug("w3m %s: Matched valid filename '%s' in current line ..", g:w3m#version, match)
      call w3m#openfile(match)
      return 1
    elseif match != ''
      call xolox#misc#msg#debug("w3m %s: File or directory '%s' doesn't exist.", g:w3m#version, match)
    endif
  endif
endfunction


function! w3m#highlight_urls() " {{{1
  " Highlight URLs and e-mail addresses embedded in source code comments.
  " URL highlighting breaks highlighting of <a href="..."> tags in HTML.
  if exists('g:syntax_on') && &ft !~ xolox#misc#option#get('shell_hl_exclude', '^\(x|ht\)ml$')
    if &ft == 'help'
      let command = 'syntax match %s /%s/'
      let urlgroup = 'HelpURL'
      let mailgroup = 'HelpEmail'
    else
      let command = 'syntax match %s /%s/ contained containedin=.*Comment.*,.*String.*'
      let urlgroup = 'CommentURL'
      let mailgroup = 'CommentEmail'
    endif
    execute printf(command, urlgroup, escape(w3m#url_pattern(), '/'))
    execute printf(command, mailgroup, escape(w3m#mail_pattern(), '/'))
    execute 'highlight def link' urlgroup 'Underlined'
    execute 'highlight def link' mailgroup 'Underlined'
  endif
endfunction


function! w3m#url_exists(url) " {{{1
  " Check whether a URL points to an existing resource (using Python).
  try
    " Embedding Python code in Vim scripts is always a bit awkward :-(
    " (because of the forced indentation thing Python insists on).
    let starttime = xolox#misc#timer#start()
python3 <<EOF

# Standard library modules.
import httplib
import urlparse

# Only loaded inside the Python interface to Vim.
import vim

# We need to define a function to enable redirection implemented through recursion.

def shell_url_exists(absolute_url, rec=0):
  assert rec <= 10
  components = urlparse.urlparse(absolute_url)
  netloc = components.netloc.split(':', 1)
  if components.scheme == 'http':
    connection = httplib.HTTPConnection(*netloc)
  elif components.scheme == 'https':
    connection = httplib.HTTPSConnection(*netloc)
  else:
    assert False, "Unsupported URL scheme"
  relative_url = urlparse.urlunparse(('', '') + components[2:])
  connection.request('HEAD', relative_url)
  response = connection.getresponse()
  if 300 <= response.status < 400:
    for name, value in response.getheaders():
      if name.lower() == 'location':
        shell_url_exists(value.strip(), rec+1)
        break
  else:
    assert 200 <= response.status < 400

shell_url_exists(vim.eval('a:url'))

EOF
    call xolox#misc#timer#stop("w3m %s: Took %s to verify whether %s exists (it does).", g:w3m#version, starttime, a:url)
    return 1
  catch
    call xolox#misc#timer#stop("w3m %s: Took %s to verify whether %s exists (it doesn't).", g:w3m#version, starttime, a:url)
    return 0
  endtry
endfunction

function! w3m#url_pattern() " {{{1
  " Get the preferred/default pattern to match URLs.
  return xolox#misc#option#get('shell_patt_url', '\<\w\{3,}://\(\(\S\&[^"]\)*\w\)\+[/?#]\?')
endfunction

function! w3m#mail_pattern() " {{{1
  " Get the preferred/default pattern to match e-mail addresses.
  return xolox#misc#option#get('shell_patt_mail', '\<\w[^@ \t\r<>]*\w@\w[^@ \t\r<>]\+\w\>')
endfunction



" vim: ts=2 sw=2 et fdm=marker
